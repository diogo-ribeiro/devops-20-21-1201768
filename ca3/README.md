# Class Assignment 3 Report


## 1. Tasks Implementation - Part 1

### 1.1. Create VM 
To set up a Virtual Machine we need to download ```VirtualBox``` and install ```Ubuntu 18.04 "Bionic Beaver"``` on it.

---
### 1.2. Clone tut_basic and gradle_basic_demo to VM
#### 1.2.1. Tut_basic installation 
Go to the virtual machine and install ```git``` running ```sudo apt install git```. After installation clone the desired project:

    % git clone https://github.com/spring-guides/tut-react-and-spring-data-rest.git

#### 1.2.2. Gradle_basic_demo installation
Go to ````ca2/part_1/```` and use ```FileZilla``` to send the folders to the Virtual Machine. 
To execute this step we need to login in the vitual machine though the FilleZilla with:
````
Host: 192.168.56.5
Username: diogo-devops
Password: <insert password>
````

---
When we are set, right click on the selected files and press ``upload``.

### 1.3. Build and execute spring boot and gradle
#### 1.3.1. Spring:Boot on tut_basic
Access this project in the virtual machine and execute the following command. Maven will automatically install all the packages and dependencies that it is needed.

    % ./mvnw spring-boot:run

#### 1.3.2. Gradle on basic_demo
Access this project in the virtual machine and before running the server we need to install the need packages and dependencies.
Run the following commands inside the machine:



- Node


    % sudo apt install nodejs
    
- Node package manager


    % sudo apt install npm

- JDK 8


    % sudo apt-get install openjdk-8-jdk


- Gradle


    % sudo apt install gradle


---
### 1.4. Access App in Host Machine
#### 1.4.1. Tut_basic
- On the Virtual Machine in ```/tut-react-*/basic```


    % ./mvnw spring-boot:run

- Use a browser in the Host Machine

    
    % http://192.168.56.5:8080/

#### 1.4.2. Gradle_basic_demo
- On the Virtual Machine in ```gradle_basic_demo```


    % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

- On the Host's terminal in ```gradle_basic_demo```


    % ./gradlew runClient

---
### 1.5. Why Host Machine usage
We need to run the Client in the Host because we only access ubuntu though the terminal, and is not possible to interact with the user interface that pops up when we run the command. We also have the limitation of only being capable of using one session at a time, disabling the possibility of running the server and the client simultaneously.

A similar scenario happens with ````./mvnw spring-boot:run````, where we can't access the browser though ubuntu and therefore check the UI and visualize the ip.

---
## 2. Tasks Implementation - Part 2

### 2.1. Create Vagrant file 
To create the vagrant file that has all the configuration needed in the virtual machine, run the following command specifying the desired project and OS on ```ca3/part_2/```.

    % vagrant init envimation/ubuntu-xenial


---
### 2.2. Set up the Vagrant file 
To add the right configurations for this project, go to this [link](https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master).
Add the following lines to the following files:

#### 2.2.1. build.gradle
```
plugins {
    (...)
    id 'war'
}

dependencies {
	(...)
	// To support war file for deploying to tomcat
	providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
}
```

---
#### 2.2.2. application.properties
```
server.servlet.context-path=/basic-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
# In the following settings the h2 file is created in /home/vagrant folder
spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
# So that spring will no drop de database on every execution.
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```

---
#### 2.2.3. app.js
- **Remove**

        % client({method: 'GET', path: '/api/employees'}).done(response => {

- **Add**
  
        % client({method: 'GET', path: '/gradle-0.0.1-SNAPSHOT/api/employees'}).done(response => {

---
#### 2.2.4. index.html
- **Remove**

        % <link rel="stylesheet" href="/main.css" />

- **Add**

        % <link rel="stylesheet" href="main.css" />

---
### 2.3. Run VM with Vagrant

#### 2.3.1. Set up Vagrant file
Go to the **Vagrant** file and add the following commands to create web and db VM.

```
# This provision is common for both VMs
config.vm.provision "shell", inline: <<-SHELL
  sudo apt-get update -y
  sudo apt-get install iputils-ping -y
  sudo apt-get install -y avahi-daemon libnss-mdns
  sudo apt-get install -y unzip
  sudo apt-get install openjdk-8-jdk-headless -y
  # ifconfig
SHELL

#============
# Configurations specific to the database VM
config.vm.define "db" do |db|
  db.vm.box = "envimation/ubuntu-xenial"
  db.vm.hostname = "db"
  db.vm.network "private_network", ip: "192.168.33.11"

  # We want to access H2 console from the host using port 8082
  # We want to connet to the H2 server using port 9092
  db.vm.network "forwarded_port", guest: 8082, host: 8082
  db.vm.network "forwarded_port", guest: 9092, host: 9092

  # We need to download H2
  db.vm.provision "shell", inline: <<-SHELL
    wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
  SHELL

  # The following provision shell will run ALWAYS so that we can execute the H2 server process
  # This could be done in a different way, for instance, setiing H2 as as service, like in the following link:
  # How to setup java as a service in ubuntu: http://www.jcgonzalez.com/ubuntu-16-java-service-wrapper-example
  #
  # To connect to H2 use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb
  db.vm.provision "shell", :run => 'always', inline: <<-SHELL
    java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
  SHELL
end

#============
# Configurations specific to the webserver VM
config.vm.define "web" do |web|
  web.vm.box = "envimation/ubuntu-xenial"
  web.vm.hostname = "web"
  web.vm.network "private_network", ip: "192.168.33.10"

  # We set more ram memmory for this VM
  web.vm.provider "virtualbox" do |v|
    v.memory = 1024
  end

  # We want to access tomcat from the host using port 8080
  web.vm.network "forwarded_port", guest: 8080, host: 8080

  web.vm.provision "shell", inline: <<-SHELL, privileged: false
    sudo apt-get install git -y
    sudo apt-get install nodejs -y
    sudo apt-get install npm -y
    sudo ln -s /usr/bin/nodejs /usr/bin/node
    sudo apt install tomcat8 -y
    sudo apt install tomcat8-admin -y
    # If you want to access Tomcat admin web page do the following:
    # Edit /etc/tomcat8/tomcat-users.xml
    # uncomment tomcat-users and add manager-gui to tomcat user

    # Change the following command to clone your own repository!
    git clone git@bitbucket.org:diogo-ribeiro/devops-20-21-1201768/ca3/part_2/tut-basic-gradle.git
    cd devops-20-21-1201768/ca3/part_2/tut-basic-gradle
    chmod u+x gradlew
    ./gradlew clean build
    # To deploy the war file to tomcat8 do the following command:
    sudo cp ./build/libs/gradle-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
  SHELL

end
```
---
#### 2.3.2. Run Vagrant
To create both Virtual Machines run the following command in the directory where you can find ```Vagrantfile```.

    % vagrant up

If something goes wrong and you need to do a cleanup for new configuration set up, run the following command to destroy the VM and repeat the process.

    % vagrant destroy -f

But if you just want to stop the VM from running on the background just insert:

    % vagrant halt

---
### 2.4. Connect to VM
To access both VM's, just insert in the terminal, in the directory where the ```Vangrantfile``` is placed:

    % vagrant ssh web

or

    % vagrant ssh db

---
### 2.5. Build and run Spring Application
After accessing with ```vagrant ssh web```, we need to build and run the application. To fullfil this goal, we can use the commands that we used so far, ````./gradlew clean build```` and ````./gradlew bootrun````. 

---
## 3. Analysis - VirtualBox alternative
When trying to test an alternative to **VirtualBox** there are plenty of solutions. One big difference is the hypervisor type. There are two types: 

  - **Type 1**: where the virtualization host software runs on what’s now known as "bare metal", which means that it runs on a computer without an operating system. Some examples are Hyper-V, Xen, or OpenVZ

  - **Type 2**: where the virtualization host software runs on an already established operating system as an application. VirtualBox fits this type as VMWare, QEMU and Parallels (macOS only). 

On this assigment I will try to implement on Parallels since I do all my work with macOS.


---
## 4. Alternative Implementation 

### 4.1. Download Parallels for MacOS
Go to the official website [here](https://www.parallels.com/products/desktop/download/) and get the image file.

---
### 4.2 Vagrant limitations
Vagrant is only **freely** available in VirtualBox and Hyper-V so it is not possible to run vagrant commands in parallels **unless** upgrading it to the **Pro edition**.
To be able to complete this implementation it was activated **15 days free trial** on **Pro Edition**.

---
### 4.3 Set up Vagrant with Parallels
Install **vagrant plugin** to use with parallels

    % vagrant plugin install vagrant-parallels

Create a new folder ````ca3/alternative```` and initiate vagrant with a different box:

    % vagrant init bento/ubuntu-18.04

After configuring **vagrantfile** run the same command defining the VM provider:

    % vagrant up --provider=parallels

### 4.4. Troubleshooting
#### 4.4.1. Same IP's and Port's
After building the other application on VirtualBox, I got some trouble setting up the new VM on parallels because I was using the same IP's so I decided to change it to a different one.

![same-ip](assets/same-ip.png)

#### 4.4.2. Stop other VM's
To be able to run on ```localhost:8080``` I had to shutdown virtualbox VM because both of them were using the same **port**.

![same-port](assets/same-port.png)

#### 4.4.3. Can't reconnect
After I stop the **Parallels** machine with ````vagrant halt```` to check the other VM, I was not able to reconnect to it again.
Even running ```vagrant up --provider=parallels``` on the root folder I got a message that the VM was already running but when inserting ```vagrant ssh web2``` or ```vagrant ssh db2``` nothing happened. I wasn't able to reconnect to them again.

![cant-reconnect](assets/no-reconnect.png)

#### 4.4.4. Tomcat don't show .war file
Even after **winning these "battles" shown previously** and following the same steps as the VirtualBox implementation I was not able to conclude the Parallels implementation.

This time I feel that I'm close to the finish line but **Tomcat** doesn't read the ````.war```` file. 

So let me explain more precisely: 

  - The build was successful (YEAH!)

![build-success](assets/build-war.png)

  - Moving ````gradle-0.0.1-SNAPSHOT```` to **Tomcat** folder was successful

![move-success](assets/move-war.png)

 - Tomcat is working

![tomcat-works](assets/tomcat-successful.png)

  - But the request fails :(

![request-fails](assets/snapshot-fail.png)

---
