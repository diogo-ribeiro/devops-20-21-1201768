# [1201768 - Diogo Ribeiro] Individual Repository for DevOps #

This repository contains the files for all the class assignemts of DevOps.

* [Tutorial](https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768/src/master/tutorial/tut-basic/)

* [Class Assignment 1](https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768/src/master/ca1/)

* [Class Assignment 2](https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768/src/master/ca2/)

* [Class Assignment 3](https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768/src/master/ca3/)

* [Class Assignment 4](https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768/src/master/ca4/)

* [Class Assignment 5](https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768/src/master/ca5/)

* [Class Assignment 6](https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768/src/master/ca6/)

**Note**
You should use Markdown Syntax so that the contents are properly rendered in Bitbucket. See how to write README files for Bitbucket in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)
