# Class Assignment 5 Report


## 1. Tasks Implementation - Part 1

### 1.1. Install and run Jenkins

To install **Jenkins** on macOS go to its section in the official [website](https://www.jenkins.io/download/lts/macos/) and run the following command:

```
  % brew install jenkins-lts
```

To start Jenkins run:

```
  % brew services start jenkins-lts
```

### 1.2. Initial setup 

After initializing Jenkins we have to find an initial password to proceed with admin creation. In my case I had to go to the terminal and run this command and insert the password on jenkins page:

```
  % cat /Users/diogoribeiro/.jenkins/secrets/initialAdminPassword
```

Next steps are:
 
  1) New item
     
  2) Insert name 
     
  3) Select pipeline
     
  4) Scroll down to pipeline section
     
  5) Select ```Pipeline script from SCM```
     
  6) In Repository URL insert ```https://diogo-ribeiro@bitbucket.org/diogo-ribeiro/devops-20-21-1201768.git```
     
  7) In Script Path insert ```ca5/part_1/tut-basic-jenkins/Jenkinsfile```

To create the script that Jenkins will read, follow [this](https://gist.github.com/atb/d23ce58350c4df272473c7fd7a96838f) structure and insert it inside the project's repository.

Jenkins file:
```
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git ' https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768.git'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                sh './gradlew assemble'
            }
        }
	stage('Test') {
            steps {
                echo 'Testing...'
                sh './gradlew test'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'build/libs/*'
            }
        }
    }
}
```

### 1.3. Build now 

Go to the main menu and press **Build Now**.

After some build fails and minor changes due to distractions, the project was still blocked in the **assemble** step.

![build-fail](part_1/assets/build-fail.png)

The logs said that Jenkins was not finding ````./gradlew```` command so I decided to go to my Jenkins file and point the directory out to make sure it run the commands in the desired place.
Basically, I edited **assemble, test and archive** stages and added the **directory** field:

```
stage('Assemble') {
    steps {
        echo 'Assembling...'
        dir('ca5/part_1/tut-basic-jenkins/') {
            sh './gradlew assemble'
        }
    }
}

stage('Test') {
    steps {
        echo 'Testing...'
        dir('ca5/part_1/tut-basic-jenkins/') {
            sh './gradlew test'
        }
    }
}

stage('Archiving') {
    steps {
        echo 'Archiving...'
        dir('ca5/part_1/tut-basic-jenkins/') {
            archiveArtifacts 'build/libs/*'
        }
    }
}
```

... and voilà, everything works perfectly.

![build-fail](part_1/assets/build-success.png)

But...

![im-stupid](part_1/assets/im-so-stupid-it-hurts.jpg)

The wrong project was used in this assigment so I had to switch to ``` gradle_basic_demo``` and refactor what was already implemented.

I noticed that the tests part was incomplete so I had **junit test report** to the tests' section.

    % junit 'build/test-results/test/TEST-basic_demo.AppTest.xml'

The Jenkinsfile final version is the following:

```
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768.git'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir('ca5/part_1/gradle_basic_demo/') {
                    sh './gradlew clean assemble'
                }
            }
        }
	    stage('Test') {
            steps {
                echo 'Testing...'
                dir('ca5/part_1/gradle_basic_demo/') {
                    sh './gradlew test'
                    junit 'build/test-results/test/TEST-basic_demo.AppTest.xml'
                }
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir('ca5/part_1/gradle_basic_demo/') {
                    archiveArtifacts 'build/libs/*'
                }
            }
        }
    }
}
```

After repeating builds because some inputs were missing as ```.war``` extension and ``junit``, jenkins managed to pass the last build with everything set up.

![final-stats](part_1/assets/final-version.png)


## 2. Tasks Implementation - Part 2

### 2.1. Set up tut-basic on Jenkins

Since some configuration from ````tut-basic-jenkins```` was already implemented in last section by mistake, I moved the repository to ``` part_2 ```.

In jenkins most steps are the same:

1) Go to `````Manage Jenkins/Manage plugins`````

2) Add following plugins:
   
    2.1) Javadoc plugin

    2.2) HTML Publisher plugin

    2.3) Docker pipeline

1) Back on dashboard, add new item

2) Insert name

3) Select pipeline

4) Scroll down to pipeline section

5) Select ```Pipeline script from SCM```

6) In Repository URL insert ```https://diogo-ribeiro@bitbucket.org/diogo-ribeiro/devops-20-21-1201768.git```

7) In Script Path insert ```ca5/part_2/tut-basic-jenkins/Jenkinsfile```


Jenkins file:
```
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768.git'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir('ca5/part_2/tut-basic-jenkins/') {
                    sh './gradlew clean assemble'
                }
            }
        }
	    stage('Test') {
            steps {
                echo 'Testing...'
                dir('ca5/part_2/tut-basic-jenkins/') {
                    sh './gradlew test'
                    junit 'build/test-results/test/TEST-main.java.com.greglturnquist.payroll.EmployeeTest.xml'
                }
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir('ca5/part_2/tut-basic-jenkins/') {
                    archiveArtifacts 'build/libs/*'
                }
            }
        }
        stage('Docker Image') {
            steps {
                echo 'Creating Docker image...'
                dir('ca5/part_2/tut-basic-jenkins/') {
                    script {
                        image = docker.build("jdccbr1201768/ca5part2:${env.BUILD_ID}")
                        image.push()
                     }
                }
            }
        }
    }
}

```

At this point I was thinking that the journey was almost at end but I had no idea what was coming next.

After pushing the second part to jenkins and press ``build now`` this error appears:

![docker-error](part_2/assets/docker-error.png)

Everything was running smooth until this error. 
Since I'm writing this part after a lot of trys without taking any printscreens, I'm just going to point out a few:

- Change Dockerfile location to ```/tut-basic-jenkins/``` 

- Edit agent to docker

- Create repository on dockerhub

- First try with credentials

- Going back to first setup 

- Minor changes on Dockerfile
  
- Edit variable name

- Uninstall docker

- Install through terminal

- Install docker-desktop again with .dmg

There were also minor changes that I don't remember, some builds were erased when I messed up the Jenkinsfile but last effort streak can be verified in the following picture:

![trys](part_2/assets/fails.png)

Thanks to **João Pinto from group 3**, I was able to fix this issue.

Basically Mac users that use Jenkins with brew need to add an environment variable to ````jenkins-lts.plist```` so Jenkins knows how to run docker commands.

![config](part_2/assets/terminal.png)

I had to find **.plist file** in Jenkins config files and add the following variable.

![variable](part_2/assets/environment-path.png)

After saving this file and restarting Jenkins...

![](part_2/assets/jenkins-restart.png)

At this point the docker section on jenkinsfile has this layout:

````
stage('Docker Image') {
   steps {
       echo 'Creating Docker image...'
       dir('ca5/part_2/tut-basic-jenkins/') {
           script {
               image = docker.build registry + ":$BUILD_NUMBER"
               docker.withRegistry( '', registryCredential ) {
                   image.push()
               }
           }
           sh "docker rmi $registry:$BUILD_NUMBER"
       }
   }
}
````

... and after pressing build now for the 10000 time the docker image is generated!!!!

![](part_2/assets/victory.png)

![](part_2/assets/docker.png)


## 3. Analysis - Alternative

After some research and colleagues feedback, I found **Buddy** alternative very interesting and easy to use.
Buddy is a web-based and self-hosted continuous integration and delivery software for git developers. Buddy uses docker containers with pre-installed languages and frameworks for builds monitoring and notification actions.

The biggest weakness of this solution is the lack of plugins but when it comes to ease of use buddy makes **Devops** and much easier job to be done.
The biggest advantage I must say is its user interface and workflow.  


## 4. Alternative Implementation

After creating account and integratin my bitbucket I followed this workflow:

NOTE: I switched ````gradle assemble```` to ````./gradle assemble```` and the rest of commands too.

![](alternative/assets/1.png)

![](alternative/assets/2.png)

![](alternative/assets/3.png)

![](alternative/assets/4.png)

![](alternative/assets/5.png)

![](alternative/assets/6.png)

![](alternative/assets/7.png)

![](alternative/assets/8.png)

![](alternative/assets/9.1.png)

![](alternative/assets/10.png)

![](alternative/assets/11.png)


I would like to thank all my teammates in group 3 for their support on this journey!

APES TOGETHER STRONG.