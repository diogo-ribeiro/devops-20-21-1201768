# Class Assignment 4 Report


## 1. Tasks Implementation

### 1.1. Settings for multi-container Application

Before creating ```dockerfile``` check [this](https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/) example.

Go to ```docker-compose.yml``` file and copy the settings for our project.

```
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.33.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data
    networks:
      default:
        ipv4_address: 192.168.33.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.33.0/24
```

### 1.2. Docker-compose file creation

In the terminal insert ```touch docker-compose.yml``` to create **project's Dockerfile** and paste in it the settings shown above.


### 1.3. Directory structure

Our project needs to follow the same structure present in the example. We will have to create **3 directories**:
    
- data
- web
- db

### 1.4. Dockerfile creation - web
Inside web directory use ````touch Dockerfile```` command in the terminal and using vim or VSCode insert the following lines in it:

```
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768.git

WORKDIR /tmp/build/devops-20-21-1201768/ca3/part_2/tut-basic-gradle/

RUN ./gradlew clean build

RUN cp build/libs/gradle-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080

```


### 1.5. Dockerfile creation - db
Now, inside db directory use ````touch Dockerfile```` command in the terminal and using vim or VSCode insert the following lines in it:

```
FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists
```

### 1.6. Reading .war file
It was supposed to be created a ````gradle-0.0.1-SNAPSHOT.war```` on ```build/libs``` and a copy on ```/usr/local/tomcat/webapps/```. After checking both locations, we can confirm that the process was correctly done. 

![libs](assets/libs.png)

![webapps](assets/webapps.png)

Since the ```.war``` file is correctly generated and copied to the right place but for some unknown reason is not rendering, I decided to clone a different git repository to double check.

### 1.7. New repository - second try

On my second try I used this git repository:

    % git clone https://bitbucket.org/Joao_Pinto_1201765/devops-20-21-1201765.git

It failed again but now the error was different. 

![ip](assets/ip-error.png)

Since I could not change the ```application.properties``` from the git repository I decided to clone it to my local machine and find a workaround.

### 1.8. Success

After cloning the repository and changing the settings on my local, I pushed to my git repository where can be found on ````ca4/tut-basic-docker````. 

I rebuilt both **docker-compose.yml**, **web - Dockerfile** and after running docker with ````docker compose up```` everything was working **perfectly**.

- docker-compose.yml
````
db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data
    networks:
      default:
        ipv4_address: 192.168.33.2
```` 


- Web: Dockerfile
````
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://diogo-ribeiro@bitbucket.org/diogo-ribeiro/devops-20-21-1201768.git

WORKDIR devops-20-21-1201768/ca4/tut-basic-docker/

RUN ./gradlew clean build

RUN cp build/libs/tut-basic-gradle-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080
````

![success](assets/success.png)

![victory](assets/victory.png)


### 1.9. Docker hub

First we need sign up for docker and get a dockerID. After validating the account we need to create a **new Repository in Docker Hub**, in this case was ```devops-web_db```. After setting up the account we go to our terminal and login with the credentials by inserting:

    % docker login

Next step is to tag our **web and db images** with **DockerID, created Repository and specific tag**.

- web
  
        % docker tag bd30d9994d63 jdccbr1201768/devops-web_db:web


- db
    
        % docker tag cd112818a20d jdccbr1201768/devops-web_db:db

To push both images to our repository we need to use push with tag:

    % docker push jdccbr1201768/devops-web_db:web

    % docker push jdccbr1201768/devops-web_db:db



### 1.10. Copy .db file from Database container

Since we already have a volume that connects our local to our db container, we just need to copy **jpadb.mv.db file** to the path.

In the ```docker-compose-yml``` we have:
```
volumes:
      - ./data:/usr/src/data
```

That means we have a ```./data``` directory in our local that connects to ````usr/src/data```` in the db container.

We just need to copy **jpadb.mv.db file** to ````usr/src/data````. To do that we must get container ID and run:

    % docker exec -it 599b5b08dcb1 cp jpadb.mv.db /usr/src/data




## 2. Analysis - Docker alternative

The selected alternative was Heroku. I already used this platform in the past when I teamed up with friends in some projects (I was a bigger noob back in that days). 

We can say that Heroku is a cloud platform as a service (PaaS) supporting several programming languages. One of the first cloud platforms, Heroku has been in development since June 2007, when it supported only the Ruby programming language, but now supports Java, Node.js, Scala, Clojure, Python, PHP, and Go. For this reason, Heroku is said to be a polyglot platform as it has features for a developer to build, run and scale applications in a similar manner across most languages.


## 3. Alternative Implementation

### 3.1. Docker compose with Heroku - Containers

Since Heroku support docker compose, once again I had to set up a multi-container application in my local environment where the images created will be pushed to Heroku.
I created a copy of ````tut-basic-docker```` directory and edited its name to ````tut-basic-heroku```` so that way I can apply any changes need to this project.

![docker-compose-heroku](assets/heroku-setup.png)

### 3.2. Containers registry and push

After signing up in Heroku, I had to login through the terminal.

    % heroku login

I had also to login to container with:

    % heroku container:login

To conclude this step, I had to move individually to each directory, **web** and **db**, and push both images to heroku with:

    % heroku container:push web

![push-web](assets/push-web.png)

![push-web](assets/push-web-success.png)

NOTE: I tried a different approach after pushing web. Check section **3.4.1.** for more info. 

... and 

    % heroku container:push db

![push-db](assets/push-db.png)

![push-db](assets/push-db-success.png)


### 3.3. Containers release and opening

The next step will be releasing both images with the following commands:

    % heroku container:release web


![release-web](assets/release-web.png)

... and

    % heroku container:release db


![release-db](assets/release-db.png)


To open the browser I had to run:

    % heroku open

... but nothing happened once again...

![heroku-open](assets/heroku-open.png)


### 3.4. Troubleshooting

#### 3.4.1 First try

I read that heroku works better with ````Postgres```` so I assumed that installing it will help me have a pre-configured database on my app. After pushing web, I decided to install postgres in heroku and open the app

![open-web](assets/open-web.png)

![postgres](assets/postgres.png)

... but an error was shown...

![heroku-open](assets/web-error.png)

![heroku-open](assets/heroku-open.png)

And just ignored what I read and went back to the step where was supposed to push both 


#### 3.4.2 Second try

After the second error I'm completely lost and just crawling around heroku...

![heroku-open](assets/activity.png)


hoping that something pops up saying that I'm a moron and giving me the solution that I deserve...

![lost-battle](assets/lost-battle.gif)

THE END



