# Class Assignment 1 Report

The source code for this assignment is located in the folder [ca1](https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768/src/master/ca1/) with the following struture:

  - **Git** implementation in [ca1/tut-basic](https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768/src/master/ca1/tut-basic/)
  - **PlasticSCM** implementation in [ca1/git-alternative/tut-basic](https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768/src/master/ca1/git-alternative/tut-basic/)

The **tasks** assigned for **this Assigment** can be found on this [link](https://bitbucket.org/diogo-ribeiro/devops-20-21-1201768/issues)

## 1. Tasks Implementation

The following steps describe the used procedure.

### 1.1. First commit 
Edit files inside ```tut-basic``` folder and insert ```git add .``` in the terminal.
Insert the command ```git commit -m "Initial commit"``` to save the added files.
To update the remote server on bitbucket you need to insert on the terminal ```git push origin master``` because we are working on ```master``` branch.

---
### 1.2. Add Tag v1.2.0
In the terminal insert ```git tag -a v1.2.0```

---
### 1.3. Commit changes
Once again insert the command ```git add .``` and ```git commit -m "fix config issues"```.

**NOTE**: I actually had some problems running the command ```./mvnw spring-boot:run``` so I had to delete everything and add every file twice to fix it.

---
### 1.4. Create new branch and move to it
To create a new branch, you need to insert ```git branch email-field``` and to move to the new branch use ```git checkout email-field```.
Instead of using these 2 commands, We could use a single one that combine both, ```git checkout -b email-field```.

---
### 1.5. Add email field
To get this project **Tree** I used ```brew install tree``` to install this functionality. By inserting ```tree``` you can see the directory tree in the terminal.

Since the project has too many folder and subfolder (node modules), it is only represented the ```src``` folder where all the changes are done.
Here is the project Tree.

```
src/
└── main
    ├── java
    │   └── com
    │       └── greglturnquist
    │           └── payroll
    │               ├── DatabaseLoader.java
    │               ├── Employee.java
    │               ├── EmployeeRepository.java
    │               ├── HomeController.java
    │               └── ReactAndSpringDataRestApplication.java
    ├── js
    │   ├── api
    │   │   ├── uriListConverter.js
    │   │   └── uriTemplateInterceptor.js
    │   ├── app.js
    │   └── client.js
    ├── resources
    │   ├── application.properties
    │   ├── static
    │   │   ├── built
    │   │   │ ├── bundle.js
    │   │   │     └── bundle.js.map
    │   │   └── main.css
    │   └── templates
    │       └── index.html
    └── tests
        └── com
            └── greglturnquist
                └── payroll
                    └── EmployeeTest.java
```

---
#### 1.5.1. Add email to file - Employee.java
Go to ```Employee.java``` and insert the following field in each section:
    
- Attributes: 
  - ```private String email;```
    

- Constructor:
  ```
    public Employee(String firstName, String lastName, String description, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;
        this.email = email;
    }
  ```
  
- Methods:
    ```
    public String getEmail() { return email; }
    
    public void setEmail(String email) { this.email = email; }
    
    @Override
    public String toString() {
        return "Employee{" +
            "id=" + id +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", description='" + description + '\'' +
            ", email='" + email + '\'' +
            '}';
    } 
    ```

---
#### 1.5.2. Add email to file - DatabaseLoader.java
Go to ```DatabaseLoader.java``` and insert the email field:
```
new Employee("Frodo", "Baggins", "ring bearer", "frodo@gmail.com")
```

---
#### 1.5.3. Add email to file - app.js
Go to ```app.js``` and insert the email field on:
```
<tr>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Description</th>
    <th>Email</th>
</tr>
```
```
<tr>
    <td>{this.props.employee.firstName}</td>
    <td>{this.props.employee.lastName}</td>
    <td>{this.props.employee.description}</td>
    <td>{this.props.employee.email}</td>
</tr>
```
---
#### 1.5.4. Tests
Created a folder called ```tests``` and marked as test directory.
All the tests are inside the file ```EmployeeTest```

---
### 1.6. Merge with master and tagging v1.3.0
Move back from ```email-field``` to ```master``` branch with the following command ```git checkout master``` to do the merge.
After moving to ```master``` insert in the command line ```git merge email-field``` and fix any conflicts that may occur.
Assign a new tag to this master version with ```git tag v1.3.0```

---
### 1.7. Bug fixing on a new branch
Create new branch and move to it with ```git checkout -b fix-invalid-email```.
Add a new validation to email as a bug fixing feature to improve our implementation.
```
public boolean validateEmailFormat(String email){
    String emailRegex = "[A-Z0-9a-z._%-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";

    Pattern pat = Pattern.compile(emailRegex);
    return pat.matcher(email).matches();
}
```

---
### 1.8. Merge bug fixing branch with master and tagging v1.3.1
Move from ```fix-invalid-email``` to ```master``` branch and merge the bug fixing branch with ```git merge fix-invalid-email```.
Assign a new tag to this master version with ```git tag v1.3.1```

---
## 2. Analysis - Git alternative

Even though **git** is the most known version control tool, there are many alternatives with different **interfaces**, **focus-group** and **repository model**.
As we know, git is a tool mostly used by **software developers** and sometimes is not very friendly for other tech roles that not code.

On this section you can find a version control alternative called **PlasticSCM** owned by **Unity**.

### 2.1. Version Control List 

![git-alternatives](./tut-basic/images/alternatives.png)  

SCOURCE:
https://www.geeksforgeeks.org/centralized-vs-distributed-version-control-which-one-should-we-choose/

### 2.2. Git VS PlasticSCM

Plastic is not based on git but it can interact with git server and git client. One big difference is that Git branches are just pointers while in Plastic branches are containers.

Plastic can work in 2 modes:
  - **Distributed** as Git by hosting its own repository and working with push/pull requests to a central server;
  - **Centralized** with workspaces and connecting directly with the server through a UI.

It was chosen the **centralized** approach.

Following this choice, we need to download the Plastic interface. The main advantages of this approach will be:
  - focused on the **user interface**;
  - **no bash commands** needed;
  - easier for **non-dev professionals** (ex: game designers).

## 3. Implementation of the Alternative

The following steps describe the used procedure.

![Plastic software](./git-alternative/tut-basic/images/pic-0.png)

### 3.1. First commit
Add ```tut-basic``` folder to ```git-alternative``` and insert in **Plastic** ```Checkin``` in the terminal.

![checkin](./git-alternative/tut-basic/images/pic-1.png)

---
### 3.2. Add Tag v1.2.0
In Plastic are called ```labels``` and are set in Changesets section

![labels](./git-alternative/tut-basic/images/pic-2.png)

---
### 3.3. Commit changes
On the side bar choose ```Pending Changes```, add some comments and press ```checkin``` to "commit".

![other-checkin](./git-alternative/tut-basic/images/pic-3.png)

---
### 3.4. Create new branch and move to it
To create a new branch, you can follow 2 different ways
1) Go to the **Terminal**, insert ```cm branch main/email-field``` and to move to the new branch use ```cm switch email-field```.

2) Go to the **Client interface** and inside **branches** section, right click on ```main``` and press ```create child branch```.

![create-branch](./git-alternative/tut-basic/images/pic-4.png)

![branch-created](./git-alternative/tut-basic/images/pic-5.png)

---
### 3.5. Add email field
On this section will be added the same code implementation that were done on **Git** in **Section 1**

![email-branch](./git-alternative/tut-basic/images/pic-6.png)

#### 3.5.1. Add email to file - Employee.java
You can check ```Employee.java``` implementation on **Section 1.5.1** from this readme

#### 3.5.2. Add email to file - DatabaseLoader.java
You can check ```DatabaseLoader.java``` implementation on **Section 1.5.2** from this readme

#### 3.5.3. Add email to file - app.js
You can check ```app.js``` implementation on **Section 1.5.3** from this readme

#### 3.5.4. Tests
You can check ```tests``` implementation on **Section 1.5.4** from this readme

---
### 3.6. Merge with master tagging v1.3.0
After ```checkin``` all changes, go to **branch explorer** and right click on **email-field branch** and select ```Merge from this changeset to``` and in the following menu **main branch**.

![merge-email](./git-alternative/tut-basic/images/pic-7.png)

After merging assign ```label v1.3.0``` by right clicking on the merge node.

![label-merge](./git-alternative/tut-basic/images/pic-8.png)

---
### 3.7. Bug fixing on a new branch
Create new branch and move into it by right clicking in ```create branch from this changeset```.

![bug-fixing-branch](./git-alternative/tut-basic/images/pic-9.png)

---
### 3.8. Merge bug fixing branch with master and tagging v1.3.1
Merge the ```fix-email branch``` by right clicking on the branch and selecting ```Merge from this changeset to``` and **main**.

![merge-bug-fixing](./git-alternative/tut-basic/images/pic-12.png)

