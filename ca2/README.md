# Class Assignment 2 Report


## 1. Tasks Implementation - Part 1

### 1.1. Download Gradle basic demo
Go to this [link](https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/) to download Gradle basic demo to folder ```ca2/part_1/``` and insert in the terminal 

    % git clone git@bitbucket.org:luisnogueira/gradle_basic_demo.git
But don't import git dependency, just the files.

---
### 1.2. Build, Run Server and Run Client
All the following terminal commands need to be run inside ```ca2/part1/gradle_basic_demo/```

#### 1.2.1. Build
To build a .jar file with the application:

    % ./gradlew build 

#### 1.2.2. Run Server
Open a terminal and execute the following command from the project's root directory:

    % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

#### 1.2.3. Run Client
Open another terminal and execute the following gradle task from the project's root directory:

    % ./gradlew runClient

---
### 1.3. Add a new task
Go to file ```build.gradle``` and insert the following task to run the server with a **gradle command** instead of ```java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001```

```
task newTaskRunServer(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "newTask is done."

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatServerApp'

    args '59001'

}
```

---
### 1.4. Add a simple unit test and update gradle
To test the ```main``` class from ```ChatServerApp```, right click inside the method, press ```generate``` and choose ```test```. The packages and the test class will be automatically created. After this step, insert the following test inside the class ```AppTest```:

```
    @Test
    public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
```

To complete this task, go to file ```build.gradle``` and insert the next line inside ```dependencies``` below the two ```implementation``` already present.

    % testImplementation group: 'junit', name: 'junit', version: '4.12'

---
### 1.5. Add a new task of type Copy to create a backup
To create a project **backup** go to ```build.gradle``` and insert the following task 
```
task copyTask(type: Copy) {
    from 'src'
    into 'build/backup'
}
```

It will create a backup from folder ```src``` and store it inside ```build/backup```.

---
### 1.6. Add a new task of type Zip to make an archive
To create a **zip file** from project, go to ```build.gradle``` and insert the following task
```
task zipTask(type: Zip) {
    from 'src'
    destinationDirectory = layout.buildDirectory.dir('archive')
    archiveFileName = 'basic_demo.zip'
}
```
It will create an archive named ```basic_demo.zip``` from folder ```src``` and store it inside ```build/archive```.

---
### 1.7. Tag as ca2-part1
After committing all the tasks to go the terminal and insert ```git tag ca2-part_1```

---
## 2. Tasks Implementation - Part 2

### 2.1. Create new branch tut-basic-gradle
Create new branch by inserting ```git checkout -b tut-basic-gradle``` in the terminal and add new directory called ```part_2```. 

---
### 2.2 Generate spring project for gradle
Go to this [link](start.spring.io) and generate a gradle spring project with the following dependencies:

- Rest Repositories
- Thymeleaf
- JPA
- H2

---
### 2.3 Move generated file to part_2
Extract file from **spring**, add it to folder **part_2** and run ```./gradlew tasks``` 

---
### 2.4 Add tut-basic source folder
Delete the generated ```src``` from spring initializer and add **tut-basic** ```src```. 

---
### 2.5 Copy extra files
Copy ```webpack.config.js``` and ```package.json``` to ```tut-basic-gradle``` and run ```npm install``` to generate ```package-lock.json```.
Delete the folder ```src/main/resources/static/built/```.

---
### 2.7 Run gradle and check localhost
After using ```./gradlew bootRun``` in the terminal and inserting ```localhost:8080``` in the browser, it will not show any frontend because it's **missing plugins** in ```build.gradle``` and ```package.json``` 

---
### 2.8 Insert frontend plugin in build.gradle
Insert the following plugin in ```plugin``` section:
    
    % id "org.siouan.frontend" version "1.4.1"

---
### 2.9 Insert frontend in build.gradle
Insert the following code in ```build.gradle``` file:
```
    frontend {
        nodeVersion = "12.13.1" 
        assembleScript = "run webpack"
    }
```

---
### 2.10 Insert webpack in package.json
Insert the following line in ```package.json``` file:
```
    "scripts": {
        "watch": "webpack --watch -d", 
        "webpack": "webpack"
    },
```

---
### 2.11 Build gradle project
Run ```./gradlew build``` to build the project with the frontend

---
### 2.12 Execute gradle project
Run ```./gradlew bootRun``` to execute the project with the frontend

---
### 2.13 Merge with master and add a tag

After testing that the Application builds and run on the web correctly, commit all changes and merge with master. To complete this task, tag master branch with **ca2-part_2**.


---


## 3. Analysis - Gradle alternative

### 3.1. Build tools
The purpose of **Build Tools** is to automate repeatable tasks in the correct, specific order and running each task accordingly. Each build tools has its limitations, process, plugins, support etc.

Looking back in time, we can check an evolution on the syntax simplicity, and a relation between older tools. 
One of the first tools to be used was **Make**, an open source _Unix utility_.

A revolution in build automation picked up high speed when apache released a build tool for **Java** called **Ant** that was supplied with built-in tasks and allowig to compile, assemble, test and run java applications. Ant was not so good managing dependencies, so apache released **Maven** to handle them and solve developers' problems. Both **Ant** and **Maven** use _XML_ as format to show the project structure. Lately, **Ivy** was created to help **Ant** with dependencies.

Microsoft also has an alternative called **MSBuild** that works similarly to **Maven** by showing project structure in _XML_ format.

**Javascript** also has build solutions to help in the frontend environment called **Gulp** and **Grunt**, very similar to each other.

Last but not least, we have **Gradle**, a combination of the best parts of **Ant** and **Maven** but a lot easier to read since it uses a _Domain Specific Language (DSL)_ instead of _XML_, making more succinct and powerful.

---
### 3.2. Please.build
Having the simplicity of **Gradle** in mind, it was found **Please** build tool, an alternative that works the same way as **Bazel**, **Buck** or **Pants** but only runs on _Linux_, _macOS_ and _FreeBSD_. All these tools are scalable and support multiple languages making them the new wave of building tools used by Big Organizations.

**Please** also uses a _Domain Specific Language (DSL)_ making it easier to read. **Please** enables build steps to be executed in a controlled hermetic environment and supports various languages as _python_, _go_, _java_, _C_ and _C++_. 

---
## 4. Please Implementation 

### 4.1. Install Please
To install Please build tool in the system run:

    % curl https://get.please.build | bash

And use ````Homebrew```` to finish the installation:

    % brew tap thought-machine/please
    % brew install please

To complete ````shell```` completion run:

    % plz --completion_script

---
### 4.2. Start Please
To start Please, go to project folder ```alternative/please+gradle-tools``` and run:

    % plz init

This command will automatically create ```.plzconfig``` and ```pleasew``` files to the root.

---
### 4.3. Create Main class and add the route to .plzconfig
Create a **Main** class and add to ```src/main/java``` since the ```src``` was imported from gradle project in ```part_2```. The class name in this project is **MainTwo** to be different from the **gradle project**.
The ```MainTwo``` class is the following:
```
import java.util.Scanner;

public class MainTwo {

    public static void main(String[] args){

        System.out.print("Inserir dois números. ");
        Scanner objOne = new Scanner(System.in);
        Scanner objTwo = new Scanner(System.in);
        double numberOne = objOne.nextDouble();
        double numberTwo = objTwo.nextDouble();
        double soma = numberOne + numberTwo;
        System.out.println(soma);

    }
}
```

For Please be able to **build** the project, it has be added the route to ```.plzconfig``` 

```
[java]
maintwo = //src/main/java
```

---
### 4.4. Add BUILD file
To be able to run ```plz build``` on the root, we need to create a **BUILD** file with ```java_binary``` that indicates the class that needs to be built.
```
java_binary(
    name = "maintwo",
    main_class = "MainTwo",
    srcs = ["MainTwo.java"],
)
```

---
### 4.5. Add a Copy task that creates a zip file
In Please the so called "tasks" are assigned as a **genrule**. To be able to run a task we have to add to the ```BUILD``` file located in ```src/main/java``` a ```genrule``` with bash commands that execute the desired feature. We need to define the file to be copied in ```srcs```, the name for the copy in ```outs``` and the bash commands in ```cmd```.
After the task is executed, the backup will be added to the path ```plz-out/gen/main/java``` as ```backup.zip```.

```
genrule(
    name = "copy",
    srcs = ["MainTwo.java"],
    outs = ["backup.zip"],
    cmd = "cp $SRCS $OUT",
)
```
