package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class EmployeeTest {

    @Test
    void validateConstructor_valid() {
        String firstName = "Tony";
        String lastName = "Ze";
        String description = "O Maior";
        String email = "tonyze@gmail.com";
        assertDoesNotThrow( () -> new Employee(firstName, lastName, description, email));
    }

    @Test
    void validateConstructor_FirstName_null() {
        String firstName = null;
        String lastName = "Ze";
        String description = "O Maior";
        String email = "tonyze@gmail.com";
        assertThrows(IllegalArgumentException.class, ()-> new Employee(firstName, lastName, description, email));
    }

    @Test
    void validateConstructor_LastName_null() {
        String firstName = "Tony";
        String lastName = null;
        String description = "O Maior";
        String email = "tonyze@gmail.com";
        assertThrows(IllegalArgumentException.class, ()-> new Employee(firstName, lastName, description, email));
    }

    @Test
    void validateConstructor_Description_null() {
        String firstName = "Tony";
        String lastName = "Ze";
        String description = null;
        String email = "tonyze@gmail.com";
        assertThrows(IllegalArgumentException.class, ()-> new Employee(firstName, lastName, description, email));
    }

    @Test
    void validateConstructor_Email_null() {
        String firstName = "Tony";
        String lastName = "Ze";
        String description = "O Maior";
        String email = null;
        assertThrows(IllegalArgumentException.class, ()-> new Employee(firstName, lastName, description, email));
    }

    @Test
    void validateConstructor_FirstName_empty() {
        String firstName = "";
        String lastName = "Ze";
        String description = "O Maior";
        String email = "tonyze@gmail.com";
        assertThrows(IllegalArgumentException.class, ()-> new Employee(firstName, lastName, description, email));
    }

    @Test
    void validateConstructor_LastName_empty() {
        String firstName = "Tony";
        String lastName = "";
        String description = "O Maior";
        String email = "tonyze@gmail.com";
        assertThrows(IllegalArgumentException.class, ()-> new Employee(firstName, lastName, description, email));
    }

    @Test
    void validateConstructor_Description_empty() {
        String firstName = "Tony";
        String lastName = "Ze";
        String description = "";
        String email = "tonyze@gmail.com";
        assertThrows(IllegalArgumentException.class, ()-> new Employee(firstName, lastName, description, email));
    }

    @Test
    void validateConstructor_Email_empty() {
        String firstName = "Tony";
        String lastName = "Ze";
        String description = "O Maior";
        String email = "";
        assertThrows(IllegalArgumentException.class, ()-> new Employee(firstName, lastName, description, email));
    }

    @Test
    void validateConstructor_FirstName_blank() {
        String firstName = "      ";
        String lastName = "Ze";
        String description = "O Maior";
        String email = "tonyze@gmail.com";
        assertThrows(IllegalArgumentException.class, ()-> new Employee(firstName, lastName, description, email));
    }

    @Test
    void validateConstructor_LastName_blank() {
        String firstName = "Tony";
        String lastName = "        ";
        String description = "O Maior";
        String email = "tonyze@gmail.com";
        assertThrows(IllegalArgumentException.class, ()-> new Employee(firstName, lastName, description, email));
    }

    @Test
    void validateConstructor_Description_blank() {
        String firstName = "Tony";
        String lastName = "Ze";
        String description = "       ";
        String email = "tonyze@gmail.com";
        assertThrows(IllegalArgumentException.class, ()-> new Employee(firstName, lastName, description, email));
    }

    @Test
    void validateConstructor_Email_blank() {
        String firstName = "Tony";
        String lastName = "Ze";
        String description = "O Maior";
        String email = "       ";
        assertThrows(IllegalArgumentException.class, ()-> new Employee(firstName, lastName, description, email));
    }

    @Test
    void validateConstructor_Email_incorrect_Format() {
        String firstName = "Tony";
        String lastName = "Ze";
        String description = "O Maior";
        String email = "tonyze@gmail";
        assertThrows(IllegalArgumentException.class, ()-> new Employee(firstName, lastName, description, email));
    }
}