/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

	// Attributes
	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private String email;

	private Employee() {}

	// Constructor
	public Employee(String firstName, String lastName, String description, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.description = description;
		this.email = email;
		validation();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return Objects.equals(id, employee.id) &&
			Objects.equals(firstName, employee.firstName) &&
			Objects.equals(lastName, employee.lastName) &&
			Objects.equals(description, employee.description) &&
			Objects.equals(email, employee.email);
	}

	// Methods
	@Override
	public int hashCode() {
		return Objects.hash(id, firstName, lastName, description, email);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmail() { return email; }

	public void setEmail(String email) { this.email = email; }

	@Override
	public String toString() {
		return "Employee{" +
			"id=" + id +
			", firstName='" + firstName + '\'' +
			", lastName='" + lastName + '\'' +
			", description='" + description + '\'' +
			", email='" + email + '\'' +
			'}';
	}

	public void validation() {
		validateFirstName();
		validateLastName();
		validateDescription();
		validateEmail();
	}

	public void validateFirstName(){
		if (this.firstName == null){
			throw new IllegalArgumentException("First Name is null");
		}
		if (this.firstName.isEmpty()){
			throw new IllegalArgumentException("First Name is empty");
		}
		if (this.firstName.trim().length() == 0){
			throw new IllegalArgumentException("First Name can not be filled with spaces");
		}
	}

	public void validateLastName(){
		if (this.lastName == null){
			throw new IllegalArgumentException("Last Name is null");
		}
		if (this.lastName.isEmpty()){
			throw new IllegalArgumentException("Last Name is empty");
		}
		if (this.lastName.trim().length() == 0){
			throw new IllegalArgumentException("Last Name can not be filled with spaces");
		}
	}

	public void validateDescription(){
		if (this.description == null){
			throw new IllegalArgumentException("Description is null");
		}
		if (this.description.isEmpty()){
			throw new IllegalArgumentException("Description is empty");
		}
		if (this.description.trim().length() == 0){
			throw new IllegalArgumentException("Description can not be filled with spaces");
		}
	}

	public void validateEmail(){
		if (this.email == null){
			throw new IllegalArgumentException("Email is null");
		}
		if (this.email.isEmpty()){
			throw new IllegalArgumentException("Email is empty");
		}
		if (this.email.trim().length() == 0){
			throw new IllegalArgumentException("Email can not be filled with spaces");
		}
		if (!validateEmailFormat(this.email)){
			throw  new IllegalArgumentException("Email is not in the correct format");
		}
	}

	public boolean validateEmailFormat(String email){
		String emailRegex = "[A-Z0-9a-z._%-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";

		Pattern pat = Pattern.compile(emailRegex);
		return pat.matcher(email).matches();
	}

}
// end::code[]
